# Supported Languages

The HPOJ supports grading in 6 languages: C, C++03, C++11, Java 8, Python 2, Python 3. All these languages are tested in production on [http://hpoj.cb.amrita.edu:8000](http://hpoj.cb.amrita.edu:8000).

As it stands, some languages are used more than others in the scope of competitive programming, so some executors have been tested more than others. As a result, they are more likely to be bug-free than the rest.