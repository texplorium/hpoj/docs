# Setting up the Judge

This installation guide is for Linux based machines (and WSL). For installation on Windows, install Visual C++ build tools before proceeding.

## Installing the prerequisites

```
$ apt install git python-dev python-pip build-essential
$ git clone <REPO URL>
$ cd judge
$ pip install -r requirements.txt
$ python setup.py develop
```

## Configuring the judge

Start by taking the `runtime` block from the output of the command `hpoj-autoconf` and put it in a new file `config.yml`. Next, add a `problem_storage_root` node where you specify where your problem data is located. 

```
problem_storage_root:
  - /judge/problems
runtime:
   ...
```

You should now be able to run `hpoj-cli -c config.yml` to enter a CLI environment for the judge.
