# Problem Data Examples
- [Standard Grading (IO-based)](/problem_examples/standard/aplusb)
- [Batched Grading (IO-based)](/problem_examples/batched/hungry)
- [Interactive Grading (conditional IO-based)](/problem_examples/interactive/seed2)
- [Signature Grading (IOI-style)](/problem_examples/signature/siggrade)
- [Generating IO Data on the Fly (large testcase generation)](/problem_examples/generator/ds3)
