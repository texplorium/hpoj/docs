# Table of Contents
- [**Linux installation**](judge/linux_installation.md)
- [**Supported Languages**](judge/supported_languages.md)
- [**Problem file format**](judge/problem_format.md)
  - [`test_cases` node](judge/problem_format.md/#test_cases)
    - [Normal Cases](judge/problem_format.md/#normal-cases)
    - [Batched Cases](judge/problem_format.md/#batched-cases)

  - [Interactive Problems - `grader` node](judge/problem_format.md/#interactive-problems-grader)
  - [Custom Checkers - `checker` node](judge/problem_format.md/#custom-checkers-checker)
  - [Checkers or Interactors for Computationally-Heavy Problems (subprocessing C++)](judge/problem_format.md/#checkers-or-interactors-for-computationally-heavy-problems)
  - [Function Signature Grading (IOI-style)](judge/problem_format.md/#function-signature-grading-ioi-style)
