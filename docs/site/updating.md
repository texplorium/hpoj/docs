# Updating the Site

The HPOJ is under active development, so occasionally you may wish to update. This is a fairly simple process.

!!! danger
    The HPOJ development team makes no commitment to backwards compatibility. It's possible that an update migration
    might add, change, or delete data from your install. Always back up before attempting an update.
    
First, switch to the site virtualenv, and pull the latest changes.

```
(hpojsite) $ git pull origin master
```

It's possible dependencies have changed since the last time you updated, so install any missing ones now.

```
(hpojsite) $ pip install -r requirements.txt
```

The database schema might also have changed, so update it.

```
(hpojsite) $ python manage.py migrate
(hpojsite) $ python manage.py check
```

Finally, update any static files that may have been changed.

```
(hpojsite) $ ./make_style.sh
(hpojsite) $ python manage.py collectstatic
(hpojsite) $ python manage.py compilemessages
(hpojsite) $ python manage.py compilejsi18n
```

That's it! You may wish to condense the above steps into a script you can run at a later time.
