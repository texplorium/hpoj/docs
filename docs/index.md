# Table of Contents

This documentation includes details on how to maintain HPOJ along with the details of how use it.

## Site
- [**Installing the Site**](site/installation.md)
- [**Updating the Site**](site/updating.md)
- [**Managing Problems Through the Site Interface**](site/managing_problems.md)

## Judge
- [**Linux Installation**](judge/linux_installation.md)
- [**Supported Languages**](judge/supported_languages.md)
- [**Problem File Format**](judge/problem_format.md)
- [**Status Codes**](judge/status_codes.md)


If in doubt, feel free to [send us a mail](mailto:codeatamrita@cb.amrita.edu).
