# Problem Data Examples

## Table of Contents
* [Standard Grading (IO-based)](/problem_examples/standard/aplusb)
* [Batched Standard Grading (IO-based)](/problem_examples/batched/hungry)
* [Custom Grading](/problem_examples/grader/shortest1)
* [Interactive Grading (conditional IO-based)](/problem_examples/interactive/game1)
* [Signature Grading (IOI-style)](/problem_examples/signature/siggrade)
* [Generating IO Data on the Fly (large testcase generation)](/problem_examples/generator/ds3)